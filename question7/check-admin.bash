#!/bin/bash

T_USER=$1;
CHECK_PERM=0;

for dir in 'dir_a' 'dir_b' 'dir_c'
do
        cd $dir
        for var in 'file1' 'file2' 'file3'
        do
      		if sudo -u $T_USER [ -r "$var" -a -w "$var" ];
                then
			echo "file $var is readable and writeable by $T_USER"
                else
			CHECK_PERM=1
               	fi
		if touch test && sudo -u $T_USER [ -e "test" ] && remove test;
                then
                        echo "can create an delete file ok"
                else
                        CHECK_PERM=1
                fi

        done
        cd ../
done
if [[ CHECK_PERM -eq 0 ]]
then
	echo "Groupe_a perm = ok"
else 
	echo "Permissions denied"
fi
