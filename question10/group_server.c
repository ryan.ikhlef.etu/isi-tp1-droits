#include <dirent.h>
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <unistd.h>
#include <arpa/inet.h>
#include <signal.h>
#include <pwd.h>
#include "../question9/pwg.h"
#define PORT 4000
  
// Driver function 


static int newSocket; 
int checkGroupID(char *name) {
	struct passwd *pwd = getpwnam(name);
	gid_t userGid = pwd->pw_gid;
	int ngroups = 0;
	getgrouplist(name, userGid, NULL, &ngroups);
	__gid_t groups[ngroups];
	getgrouplist(name, userGid, groups, &ngroups);

	for(int n = 0; n < ngroups; n++) {
        	if(userGid == groups[n]) {
            		return 0;
        	}
    	}

	return 1;	
}

int areSamePassword2(char *pass, char *oldpass) {
	return strncmp(pass, oldpass, strlen(pass));
}

int main() { 
    int socketfd, ret;
    struct sockaddr_in servaddr;
    struct sockaddr_in newaddr;

    socklen_t addr_size;

    char buffer[1024];
    pid_t pid;
  
    // socket create and verification 
    socketfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (socketfd < 0) { 
        perror("socket creation failed...\n"); 
        exit(1); 
    } 
    printf("Socket successfully created..\n");


    memset(&servaddr, '\0', sizeof(servaddr));
  
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
    servaddr.sin_port = htons(PORT);


    //bind socket
    ret = bind(socketfd, (struct sockaddr*)&servaddr, sizeof(servaddr));
	if(ret < 0){
		perror("Binding failed.\n");
		exit(1);
	}
	printf("Bind to port  %d\n", PORT);

	if(listen(socketfd, 10) == 0){
		printf("Listening....\n");
	}else{
		perror("Error in binding.\n");
	}


	while(1){
		newSocket = accept(socketfd, (struct sockaddr*)&newaddr, &addr_size);
		if(newSocket < 0){
			exit(1);
		}
		printf("Connection accepted from %s:%d\n", inet_ntoa(newaddr.sin_addr), ntohs(newaddr.sin_port));
		recv(newSocket, buffer, 1024, 0);	
		bzero(buffer, sizeof(buffer));
		recv(newSocket, buffer, 1024, 0);
		printf("buffer :=%s\n", buffer);
		char name[1024];
		strcpy(name, buffer);
                bzero(buffer, sizeof(buffer));

		recv(newSocket, buffer, 1024, 0);
		//parse le buffer pour get password + id
		
		printf("buffer :=%s\n", buffer);
		char password[1024];
		strcpy(password, buffer);

		bzero(buffer, sizeof(buffer));
		//check si password et id correct
		
        	if(checkGroupID(name) == 1) {
        		printf("Vous n'appartennez pas au bon groupe.\n");
      			close(newSocket);
    		}
		int found=1;
    		if (check_password_exists(name) == 0) {
        		
    			char *line = NULL;
    			size_t len = 0;
    			FILE *fd_admin;
			fd_admin = fopen("/home/admin/passwd", "r");
        		if(fd_admin != NULL) {
            			while (getline(&line, &len, fd_admin) != EOF && found == 1) {
               				printf("line : %s\n", line);
					if(strstr(line, name) != NULL && areSamePassword2(password, strchr(line, ':') + 1) == 0) {
                   				found = 0;
                   				printf("found password \n");
               				}
            			}
        		}
		}

		if(found == 0 && (pid = fork()) == 0){
			close(socketfd);
			printf("Login success\n");
			struct passwd *pwd =getpwnam(name);
			if(pwd == NULL) close(newSocket);

			seteuid(pwd->pw_uid);
			setegid(pwd->pw_gid);
			char *command;
			char param[1024];
			struct dirent *lecture;
			DIR *rep;
			while(1){
				recv(newSocket, buffer, 1024, 0);
				if(strcmp(buffer, ":exit") == 0){
					printf("Disconnected from %s:%d\n", inet_ntoa(newaddr.sin_addr), ntohs(newaddr.sin_port));
					break;
				}else {
					printf("Client: %s\n", buffer);
					printf("list : %s\n", strstr(buffer, "list"));
					if(strstr(buffer, "list") != NULL) {
						command = "ls";
						for(int c = 5; c < strlen(buffer); c++)
							param[c - 5] = buffer[c];
						printf("param : %s\n", param);
						if(strstr(param, "dir_a") == NULL || strstr(param, "dir_b") == NULL  ||strstr(param, "dir_c") == NULL ) printf("Your param need to be dir_a, dir_b or dir_c\n");
						else {
						printf("param : %s\n", param);	
							 rep = opendir(param);
							 if(rep == NULL) {
								 printf("cannot open directory '%s'\n", param);
								 break;
							 }
							 while ((lecture = readdir(rep)) != NULL) {
							  printf("%s\n", lecture->d_name);
								 send(newSocket, lecture->d_name, strlen(lecture->d_name), 0);
   							 }
 						         closedir(rep);
						
//							execlp(command, param);
						}

					} else if(strstr(buffer, "read") != NULL) {
						command = "cat";
                                                for(int c = 4; c < strlen(buffer); c++)
                                                        param[c - 4] = buffer[c];

						 //close(1);
                                                // dup(newSocket);
                                                 execlp(command, param);
					} else {
						printf("enter a command between : list, read and close");					
						break;
				       	}


					send(newSocket, buffer, strlen(buffer), 0);
					bzero(buffer, sizeof(buffer));
				}
			}
		} else {
		    close(newSocket);
		}

	}
	printf("socket close\n");
	close(newSocket);

	return 0;
    
} 
