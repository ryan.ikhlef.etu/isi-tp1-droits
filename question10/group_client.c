#include <netdb.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> 
#include <signal.h>

#include <assert.h>
#define PORT 4000 
static int socketClient;

int main(int argc, char** argv ) 
{	
   assert (argc == 2);
   FILE *fd;
   size_t len = 0;
   char *password;
   char *name;

   printf("avnt\n");
   fd = fopen(argv[1], "r");
   if(fd != NULL) {
       	   getdelim(&name, &len, ' ', fd);
	   printf("name : %s\n", name);
	   len = 0;
           getdelim(&password, &len, '\n', fd);
	   password[strlen(password) - 1] = 0;
	   printf("password : %s\n", password);
	   name[strlen(name)-1] = 0;
   }
   printf("après\n");

   int ret; 
   struct sockaddr_in servaddr;
   char buffer[1024]; 
  
    // socket create and varification 
    socketClient = socket(AF_INET, SOCK_STREAM, 0); 
    if (socketClient <0) { 
        printf("socket creation failed...\n"); 
        exit(1); 
    } 
    else printf("Socket successfully created..\n"); 
    memset(&servaddr, '\0', sizeof(servaddr)); 
  
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    servaddr.sin_port = htons(PORT);

    ret = connect(socketClient,(struct sockaddr*)&servaddr, sizeof(servaddr));
  
    // connect the client socket to server socket 
    if (ret <0) { 
        printf("connection with the server failed...\n"); 
        exit(1); 
    } 
    else
        printf("connected to the server..\n"); 

    //vide le buffer ?
    printf("sending credentials\n");
    strcpy(buffer, argv[0]);
    send(socketClient, buffer, strlen(buffer), 0);
 
    //send username
    strcpy(buffer, name);
    send(socketClient, buffer, strlen(buffer), 0);
    
    //send password
    strcpy(buffer, password);
    send(socketClient, buffer, strlen(buffer), 0);
    char *line;
    len = 0;
    while(getline(&line, &len, fd) != EOF){
	printf("line : %s\n", line);
	if(strstr(line, "list") || strstr(line, "read")) {
		printf("ok\n");
	       send(socketClient, line, strlen(line), 0);
	}
        else if(strstr(line, "close")) break;
	else printf("commande non reconnue : %s\n", line);
	
	if(recv(socketClient, buffer, 1024, 0) < 0){
		printf("Error in receiving data.\n");
	}else{
		printf("Server: \t%s\n", buffer);
	}
    }

    close(fd);
    close(socketClient);
    printf("Disconnected from server.\n");
    return 0;
} 
