# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- IKHLEF, Ryan email: ryan.ikhlef.etu@univ-lille.fr

- DELBROUCQ, Alix, email: alix.delbroucq.etu@univ-lille.fr

## Question 1

Question 1:

Il n'a pas le droit d'ouvrir en lecture le fichier car le propriétaire est toto et que toto n'a pas les droits. 
Si le propriétaire était ubuntu alors toto aurait pu ouvrir le fichier étant donné qu'il appartient au groupe. 

## Question 2

 * Le x nous indique si le fichier est executable. 
 * On ne peut pas parcourir le repertoire parce que le repertoire car nous n'avons pas les droits pour exécuter le dossier.
 * On obtient le resultat ci-dessous car il ne peut pas entrer dans le répertoire mais il a tout de même les droits de lecture. 
```
 d????????? ? ? ? ?            ? .
 d????????? ? ? ? ?            ? ..
 -????????? ? ? ? ?            ? data.txt
```


## Question 3

1. Exécution de suid sans le tag :

```
euid = 1001
egid = 1001
ruid = 1001
rgid = 1001
Cannot open file: Permission denied

```

1001 correspond bien aux id de toto
uid=1001(toto) gid=1001(toto) groups=1001(toto),1000(ubuntu)
Mais nous ne pouvons pas lire ni ouvrir le contenu du fichier. 


2. Exécution de uid après avoir exécuter la commande `chmod u+s ` 
```
euid = 1000
egid = 1001
ruid = 1001
rgid = 1001
Hello world
```

On peut voir que l'euid est maintenant de 1000 (ce qui correspond à l'id user de ubuntu). 
ubuntu ayant les permissions pour lire le fichier nous pouvons afficher le contenu de data.txt 

On remarque par ailleurs que si on exécute la commande `chmod +s` alors l'id du groupe (egid) et du user (euid) sont à l'id d'ubuntu (1000) car c'est le propriétaire de l'executable. 


## Question 4
Le tag ne semble pas être efficace. 

uid: 1001
euid: 1001
gid: 1001
egid: 1001

## Question 5

Voici un extrait du fichier `/etc/passwd` : 

    uuidd:x:107:112::/run/uuidd:/usr/sbin/nologin
    tcpdump:x:108:113::/nonexistent:/usr/sbin/nologin
    sshd:x:109:65534::/run/sshd:/usr/sbin/nologin
    landscape:x:110:115::/var/lib/landscape:/usr/sbin/nologin
    pollinate:x:111:1::/var/cache/pollinate:/bin/false
    systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
    ubuntu:x:1000:1000:Ubuntu:/home/ubuntu:/bin/bash
    lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
    toto:x:1001:1001:,,,:/home/toto:/bin/bash


La commande `chfn` permet de modifier les informations d'un utilisateur ; Son nom, 
prénom, son numéro de téléphone...

Le résultat de la commande `ls -al /usr/bin/chfn` est : 

    -rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn

On remarque que le propriétaire de ce fichier est **root** et que les droits pour l'utilisateur est l'écriture et la lecture. Cependant, le système peut exécuter ce fichier (d'où la mention du *s* dans les droits utilisateurs).

Après avoir exéctuté la commande `chfn` voici le même extrait du fichier `/etc/passwd` :

    uuidd:x:107:112::/run/uuidd:/usr/sbin/nologin
    tcpdump:x:108:113::/nonexistent:/usr/sbin/nologin
    sshd:x:109:65534::/run/sshd:/usr/sbin/nologin
    landscape:x:110:115::/var/lib/landscape:/usr/sbin/nologin
    pollinate:x:111:1::/var/cache/pollinate:/bin/false
    systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
    ubuntu:x:1000:1000:Ubuntu:/home/ubuntu:/bin/bash
    lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
    toto:x:1001:1001:,10,0612483455,:/home/toto:/bin/bash

On remarque, sur la dernière ligne, toto à bien de nouvelles informations (son numéro de salle : *10* et son numéro de téléphone : *0612483455*). En revanche, on ne peut pas modifier son nom lors que nous sommes connectés en tant que toto.


## Question 6

Tout le monde peut afficher le contenu du fichier `/etc/passwd` c'est pourquoi les mots de passes ne sont pas stockés là. 
Ils semblent stockés dans le fichier : `/etc/shadow` en cryptés.

## Question 7

Le script create_groupe_and_user permet de créer les utilisateurs lambda_a, lambda_b et admin. 
Il créait les groupes groupe_a, groupe_b, groupe_c, special. 
lambda_a a pour groupe -> groupe_a, groupe_c
lambda_b a pour groupe -> groupe_b, groupe_c
admin -> special

le script create permet de créer la structure suivante :

```
├── dir_a
│   ├── file1
│   ├── file2
│   ├── file3
│   └── test
├── dir_b
│   ├── file1
│   ├── file2
│   ├── file3
│   └── test
└── dir_c
    ├── file1
    ├── file2
    └── file3
```

avec pour droits : 

`ls -al `

drwsrws--T 2 admin  groupe_a 4096 Jan 26 22:17 dir_a
drwsrws--T 2 admin  groupe_b 4096 Jan 26 22:10 dir_b
drwsr-s--- 2 admin  groupe_c 4096 Jan 19 15:15 dir_c

`ls -al dir_a`

-rwxrw-r-- 1 admin  groupe_a   13 Jan 19 15:15 file1
-rwxrw-r-- 1 admin  groupe_a   13 Jan 19 15:15 file2
-rwxrw-r-- 1 admin  groupe_a   13 Jan 19 15:15 file3

`ls -al dir_b `

-rwxrw-r-- 1 admin  groupe_b   13 Jan 19 15:15 file1
-rwxrw-r-- 1 admin  groupe_b   13 Jan 19 15:15 file2
-rwxrw-r-- 1 admin  groupe_b   13 Jan 19 15:15 file3

`ls -al dir_c`

-rwxr-xr-- 1 admin  groupe_c   13 Jan 19 15:15 file1
-rwxr-xr-- 1 admin  groupe_c   13 Jan 19 15:15 file2
-rwxr-xr-- 1 admin  groupe_c   13 Jan 19 15:15 file3

On peut donc voir ici que comme admin est le propriétairede chaque fichier et de chaque dossier il a tous les droits. 
Le tag sticky-bit est appliqué sur les dossiers dir_a et dir_b ce qui veut dire que un utilisateur ne peut pas supprimer ou renommer un fichier qui ne lui appartient pas. 
le tagS permet à tous les fichiers qui seront créés dans le dossier dir_a ou dir_b d'avoir le même groupe que leur dossier parent. 

Pour vérifier les permission du groupe a il faut lancer le script :
`check-group_a.bash <username>`

Pour vérifier les permission du groupe a il faut lancer le script :
`check-group_b.bash <username>`

Pour vérifier les permission du groupe a il faut lancer le script :
`check-group_c.bash <username>`

Ces fichiers tests la lecture, l'ecriture et la création et modification d'un fichier sur l'architecture ci-dessus. 


## Question 8

Le programme et les scripts dans le repertoire *question8*.

Il faut compiler les sources en utilisant la commande `make question8` à la racine du dossier isi-tp1-droits. 

`./rmg <fichier à supprimer>`

## Question 9

Le programme et les scripts dans le repertoire *question9*.

Il faut compiler les sources en utilisant la commande `make question9` à la racine du dossier isi-tp1-droits. 

`./main <name_user>`

Attention : un fichier /home/admin/passwd est créé ainsi qu'un fichier ./passwd2 qui est ensuite supprimer. Le fichier passwd2 permet de modifier le fichier les mots de passes du fichier /home/admin/passwd. 
Les mots de passes sont encrypter via la fonction crypt. 
(cf. https://ftp.gnu.org/old-gnu/Manuals/glibc-2.2.3/html_chapter/libc_32.html).


## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests.

Il faut compiler les sources en utilisant la commande `make question10 && make question10bis` à la racine du dossier isi-tp1-droits.
dans le répertoire question 10. 

Dans un terminal lancer le serveur. 
`./group_server`

Ce serveur peut gérer plusieurs clients simultanément. 

Dans d'autres terminaux lancer les clients : 

`./group_client <chemin vers un fichier>`

Nous avons réussi la partie gestions des connexions ainsi que la partie authentification. Seulement nous rencontrons un problème pour exécuter les commandes du fichier dans la partie serveur.  






