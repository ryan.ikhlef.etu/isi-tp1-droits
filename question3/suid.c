#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>


int main(int argc, char *argv[]) {
    printf("euid = %d", geteuid()); 
    printf("\negid = %d", getegid()); 
    printf("\nruid = %d", getuid()); 
    printf("\nrgid = %d\n", getgid()); 

    char chaine[512] = "";
    
    FILE* fd = fopen("toto/data.txt", "r");
    if (fd == NULL){
	    perror ("Cannot open file");
	    exit(EXIT_FAILURE);
    }
    while(fgets(chaine, 512, fd) != NULL) {
        printf("%s", chaine);
    }
    fclose(fd);

    return 0;
}
