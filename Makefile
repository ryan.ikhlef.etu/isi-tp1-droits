ALL = question3/suid question8/rmg question9/pwg question10/group_server question10/group_client
option = -Wall -Wextra -D_XOPEN_SOURCE=500

compile : $(ALL)
question3 : question3/suid
question8 : question8/rmg
question9 : question9/pwg
question10 : question10/group_server 
question10bis : question10/group_client

question3/suid : question3/suid.c
	gcc $(option) -g  $^ -o $@

question8/rmg : question8/rmg.c question8/check_pass.h question8/check_pass.c
	gcc $(option) -g  $^ -o $@
	chmod u+s $@

question9/pwg : question9/main.c question9/pwg.c question9/pwg.h
	gcc $(option) -g $^ -o $@ -lcrypt
	chmod u+s $@

question10/group_server : question10/group_server.c question9/pwg.c
	gcc $(option) -g $^ -o $@ -lcrypt
	chmod u+s $@
question10/group_client: question10/group_client.c 
	gcc $(option) -g $^ -o $@

clean : 
	rm -f question3/*.o question3/suid
	rm -f question8/*.o question8/rmg
	rm -f question9/*.o question9/pwg
	rm -f question10/*.o question10/group_server question10/group_client
