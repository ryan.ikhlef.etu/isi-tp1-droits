#include "check_pass.h"

int main(int argc, char** argv) {
    if(argc >= 1 && argc < 2) {
        printf("Veuillez indiquez le fichier à supprimer.\n");
	return -1;
    }

    char* pathname = argv[1];
//    char* name = argv[2];

    uid_t t = getuid();
   
    struct passwd *pwd = getpwuid(t);
    if(pwd == NULL) return -1;

    char *name = pwd->pw_name;
    int res = check_identity(pathname, name);

    if (res == 0) {
        remove(pathname);
        printf("%s supprimé avec succès.\n", pathname);
        return 0;

    } else {
        return res;
    }
    
} 
