#!/bin/bash
rm -rf dir_a dir_b dir_c
mkdir dir_a dir_b dir_c
sudo chgrp groupe_a dir_a
sudo chgrp groupe_b dir_b
sudo chgrp groupe_c dir_c
sudo chown admin dir_a
sudo chown admin dir_b
sudo chown admin dir_c
sudo chmod 770 dir_a
sudo chmod 770 dir_b
sudo chmod 750 dir_c
sudo chmod +t dir_a
sudo chmod +t dir_b
sudo chmod +s dir_a
sudo chmod +s dir_b
sudo chmod +s dir_c
for dir in 'dir_a' 'dir_b' 'dir_c'
do
	cd $dir
	for var in 'file1' 'file2' 'file3'
	do 
		sudo touch $var
		sudo echo "coucou $var" >> $var
		sudo chown admin $var
		if [ $dir = "dir_c" ]
		then
		       sudo chmod 754 $var

		else
			sudo chmod 764 $var
		fi
	done
	cd ../
done
