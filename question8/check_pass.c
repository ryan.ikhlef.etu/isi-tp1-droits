#include "check_pass.h"

/**
 * Asks password to the user and checks if he is correct.
 *
 * @param name The username of the user.
 * @return 0 if the password is correct, 1 if password is bad and -1 if the admin file is not create.
 */
int check_password(char *name) {
    char password [PASSWORD_MAX_SIZE];
    ssize_t read;
    char * line = NULL;
    size_t len = 0;
    int found = 1;

    FILE *fd_admin;
    fd_admin = fopen("/home/admin/passwd", "r");

    if(fd_admin != NULL) {
        printf("Saisissez mot de passe : ");
        scanf("%s", password);

        while ((read = getline(&line, &len, fd_admin)) != EOF && found == 1) {
            if(strstr(line, name) != NULL && strstr(line, password) != NULL) {
		        found = 0;
            }
        }

        if(found != 0) {
            return 1;
        } else {
            return 0;
        }
    }
    return -1;
}

/**
 * Checks if the group id given in params is on the groups array.
 * 
 * @param gid The group id that we want to compare.
 * @param groups The address of the first element in a array of group id.
 * @param ngroups The size of the groups tab.
 *
 * @return 0 if the groups array contains one element gid equals to gid given in params.
 */
int isGroupIdSame(gid_t gid, gid_t *groups, int ngroups) {
    for(int n = 0; n < ngroups; n++) {
        if(gid == groups[n]) {
            return 0;
        }
    }

    return 1;
}

/**
 * Checks if the user have the group required to delete the file.
 *
 * @param pathname The path to check folder permissions.
 * @param name The username of the user.
 * @return 0 if the user have permissions, 1 otherwise.
 */
int check_groupID(char *pathname, char* name) {
    struct stat buf;
    int ngroups = 0;

    stat(pathname, &buf);

    gid_t *list;
    ngroups = getgroups(0, NULL);
    int t = getgroups(ngroups, list);

    return isGroupIdSame(buf.st_gid, list, ngroups);

}

/**
 * Checks identity of the user. That mean, checks if the user have a group which have the permission to delete the file and if the user have the good association of name and password.
 *
 * @param pathname The path of the file.
 * @param name The username of the user.
 * @return 1 if the user have bad group id, 2 if the user have bad association of name/password and 0 if the user check his identity successfully.
 */
int check_identity(char *pathname, char *name) {
    if(check_groupID(pathname, name) == 1) {
        printf("Vous n'appartennez pas au bon groupe.\n");
        return 1;
    }
    
    if(check_password(name) == 1) {
        printf("Mauvais mot de passe.\n");
        return 2;
    }

    return 0;

}
