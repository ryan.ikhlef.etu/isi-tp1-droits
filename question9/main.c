#include "pwg.h"

int main(int argc, char** argv) {
    if(argc >= 1 && argc < 2) {
        printf("Veuillez indiquez votre nom d'utilisateur.\n");
	return -1;
    }

    char* name = argv[1];

    int res = check_identity2(name);

    if (res == 0) {

        printf("Mot de passe modifié avec succès.\n");
        return 0;

    } else {
        return res;
    }
    
} 
