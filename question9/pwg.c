#include "pwg.h"
#include <unistd.h>
#include <time.h>

char *encrypter(char *password);
int areSamePassword(char *password, char *hashedPassword);
/**
 * Asks password to the user and checks if he is correct.
 *
 * @param name The username of the user.
 * @return 0 if the password is correct, 1 if password is bad and -1 if the admin file is not create.
 */

int checkOldPassword(char * name){
    char password [PASSWORD_MAX_SIZE];
    char *line = NULL;
    size_t len = 0;
    int found = 1;
    int cpt=0;
    FILE *fd_admin;

    fd_admin = fopen("/home/admin/passwd", "r");
    while (cpt<5 && found == 1){
    	if(fd_admin != NULL) {
            printf("Saisissez votre ancien mot de passe : ");
            scanf("%s", password);
	    
    	    while (getline(&line, &len, fd_admin) != EOF && found == 1) {
	       if(strstr(line, name) != NULL && areSamePassword(password, strchr(line, ':') + 1) == 0) {
                   found = 0;
		   printf("found password \n");
               }
            }
          cpt++;
	  fseek(fd_admin, 0, SEEK_SET);
	}
    }
    if (cpt >=5) exit(EXIT_FAILURE);

    if(found != 0) {
        return 1;
    } else {
        return 0;
    }
    return -1;
}

char* encrypter(char *password) {
    unsigned long seed[2];
    char salt[] = "$1$ierfdjef";
    const char *const seedchars =
    "./0123456789ABCDEFGHIJKLMNOPQRST"
    "UVWXYZabcdefghijklmnopqrstuvwxyz";
    int i;

    /* Generate a (not very) random seed.  
     You should do it better than this... */
    seed[0] = time(NULL);
    seed[1] = getpid() ^ (seed[0] >> 14 & 0x30000);
    for (i = 0; i < 8; i++)
        salt[3+i] = seedchars[(seed[i/5] >> (i%5)*6) & 0x3f];

    return crypt(password, salt);
}

int areSamePassword(char *password, char *hashedPassword) {
	char * result;
	result = crypt(password, hashedPassword);
	return (strncmp(result, hashedPassword, strlen(result)) == 0 ? 0 : 1);
}
int newPassword(char * name,int old){
    char password [PASSWORD_MAX_SIZE];
    ssize_t read;
    char * line;
    size_t len = 0;
    char *newPassword;
     
    FILE *fd_admin, *fd_dest;
    fd_admin = fopen("/home/admin/passwd", "r");
    fd_dest = fopen("./passwd2", "a+");

    if(fd_admin != NULL) {
        printf("Saisissez votre nouveau mot de passe : ");
        scanf("%s", password);
	newPassword = encrypter(password);
        while ((read = getline(&line, &len, fd_admin)) != EOF) {
		if(strstr(line, name) != NULL) {
		    fprintf(fd_dest, "%s:%s\n", name, newPassword);

            } else { 	
		   fprintf(fd_dest, "%s", line);
	    }

        }

       	if (old ==1) fprintf(fd_dest, "%s:%s\n", name, newPassword);
	

        fclose(fd_dest);
	fclose(fd_admin);
	remove("/home/admin/passwd");
	rename("./passwd2", "/home/admin/passwd");
	return 0;
    }
    return -1;

}



int check_password_exists(char * name) {
    ssize_t read;
    char * line = NULL;
    size_t len = 0;
    int found = 1;

    FILE *fd_admin;
    fd_admin = fopen("/home/admin/passwd", "r");

    if(fd_admin != NULL) {
       while ((read = getline(&line, &len, fd_admin)) != EOF && found == 1) {
            if(strstr(line, name) != NULL) {

                        found = 0;
            }
        }
    }
    if(found != 0) return 1;
    else return 0;
    return -1;
}



/**
 * Checks if the group id given in params is on the groups array.
 *
 * @param gid The group id that we want to compare.
 * @param groups The address of the first element in a array of group id.
 * @param ngroups The size of the groups tab.
 *
 * @return 0 if the groups array contains one element gid equals to gid given in params.
 */
int isGroupeIdFromGroupeAorGroupeB(gid_t* groups, int ngroups) {
    struct group* grpA= getgrnam ("groupe_a");
    gid_t idGrpA= grpA->gr_gid;
    struct group* grpB = getgrnam("groupe_b");
    gid_t idGrpB= grpB->gr_gid;

   
    for(int n = 0; n < ngroups; n++) {
        if(idGrpA == groups[n] || idGrpB == groups[n] ) {
            return 0;
        }
    }

    return 1;
}

/**
 * Checks if the user have the group required to delete the file.
 
 * @param pathname The path to check folder permissions.
 * @param name The username of the user.
 * @return 0 if the user have permissions, 1 otherwise.
 */
int check_groupID(char* name) {
    int userGrpID;
    int ngroups = 0;

    userGrpID = getegid();

    getgrouplist(name, userGrpID, NULL, &ngroups);
    __gid_t groups[ngroups];
    getgrouplist(name, userGrpID, groups, &ngroups);

    return isGroupeIdFromGroupeAorGroupeB((gid_t*)groups, ngroups);

}

/**
 * Checks identity of the user. That mean, checks if the user have a group which have the permission to delete the file and if the user have the good association of name and password.
 *
 * @param name The username of the user.
 * @return 1 if the user have bad group id, 2 if the user have bad association of name/password and 0 if the user check his identity successfully.
 */

int check_identity2(char *name) {
    int old =1;
    if(check_groupID(name) == 1) {
        printf("Vous n'appartennez pas au bon groupe.\n");
        return 1;
    }
    
    if (check_password_exists(name) == 0) {
        if (checkOldPassword(name) == 0) {
            old=0;
            newPassword(name, old);
        }
    } else {
        newPassword(name, old);
    }
    return 0;
}
