#!/bin/bash

T_USER=$1;
CHECK_PERM=0;

for dir in 'dir_a' 'dir_b' 'dir_c'
do
        cd $dir
        for var in 'file1' 'file2' 'file3'
        do
		if [ $dir = "dir_a" ]
		then	
                	if sudo -u $T_USER [ -r "$var" -a -w "$var" ];
                	then
				echo "file $var from dir_a is readable ans writeable by $T_USER"

                	else
				CHECK_PERM=1
                	fi
			if touch test && sudo -u $T_USER [ -e "test" ] && remove test;
                        then
                                echo "can create an delete file ok"
                        else
				CHECK_PERM=1 
                        fi

		fi
		if [ $dir = "dir_b" ]
		then 
			if sudo -u $T_USER [ -r "$var" -o -w "$var" ];
			then
				CHECK_PERM=1
			else
				echo "$T_USER can't read or write file from dir_b"
			fi
			
			if touch test && sudo -u $T_USER [ -e "test" ] && remove test;
                        then
                                CHECK_PERM=1
                        else
                                echo "can't create an delete file ok"
                        fi

		fi
		if [ $dir = "dir_c" ]
		then
			if sudo -u $T_USER [ -r "$var" ];
			then
				if sudo -u $T_USER [ -w "$var" ];
				then
					CHECK_PERM=1
				else
					echo "read only for dir_c ok"
				fi
			else
				CHECK_PERM=1
			fi
		fi


        done
        cd ../
done
if [[ CHECK_PERM -eq 0 ]]
then
	echo "Groupe_a perm = ok"
else 
	echo "Permissions denied"
fi
