#ifndef CHECK_PASS_H_
#define CHECK_PASS_H_
#define PASSWORD_MAX_SIZE 256

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <grp.h>
#include <pwd.h>

/**
 * Checks identity of the user. That mean, checks if the user have a group which have the permission to delete the file and if the user have the good association of name and password.
 *
 * @param pathname The path of the file.
 * @param name The username of the user.
 * @return 1 if the user have bad group id, 2 if the user have bad association of name/password and 0 if the user check his identity successfully.
 */
int check_identity(char *pathname, char *name);

int check_password_exists(char* name);

int check_groupID(char* pathname, char* name);

int  areSamePassword (char* password, char* hashPassword);


#endif
