#ifndef PWG_H_
#define PWG_H_
#define PASSWORD_MAX_SIZE 256
#define  _GNU_SOURCE
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <grp.h>

int check_password_exists(char* name);

int check_groupID(char* name);

int areSamePassword (char* password, char* hashPassword);

int check_identity2(char* name);


#endif

